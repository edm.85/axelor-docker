#!/usr/bin/env bash

AXELOR_RELEASES=`curl --silent "https://api.github.com/repos/axelor/axelor-open-suite/releases"  | jq -r .[].tag_name | sort | tr '\n' ' '`
PMOSCODE_RELEASES=`curl --silent "https://hub.docker.com/v2/repositories/pmoscode/axelor-aio-erp/tags?page_size=1024" | jq -r .results[].name | sort | tr '\n' ' '`

AXELOR_RELEASES_ARRAY=("${AXELOR_RELEASES}")
PMOSCODE_RELEASES_ARRAY=("${PMOSCODE_RELEASES}")

echo axelor versions: ${AXELOR_RELEASES_ARRAY}
echo pmoscode versions: ${PMOSCODE_RELEASES_ARRAY}

# Remove duplicates from 'AXELOR_RELEASES_ARRAY'...
for versions in ${PMOSCODE_RELEASES_ARRAY[@]}; do
    AXELOR_RELEASES_ARRAY=( "${AXELOR_RELEASES_ARRAY[@]/$versions}" )
done

echo " "
echo Umtagged versions: ${AXELOR_RELEASES_ARRAY[@]}
echo " "

for value in ${AXELOR_RELEASES_ARRAY}
do
    echo Tagging version: ${value}
    git tag -a ${value} -m "New Version $value"; git push https://oauth2:${REPO_ACCESS_TOKEN}@gitlab.com/pmoscode/axelor-docker.git HEAD:master --tags
done
